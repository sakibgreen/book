<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Books
Route::get('/','BookController@index')->name('books.index');
Route::get('create','BookController@create')->name('books.create');
Route::post('books','BookController@store')->name('books.store');
Route::get('books/{id}/show','BookController@show')->name('books.show');
Route::get('/manage','BookController@manage')->name('books.manage');
Route::get('books/{book}/edit','BookController@edit')->name('books.edit');
Route::put('/books/{id}','BookController@update')->name('books.update');
Route::delete('books/{book}','BookController@destroy')->name('books.destroy');

//Comments
Route::post('/books/{book}/comment','CommentController@store')->name('comments.store');
Route::get('/books/{book}/comment','CommentController@show')->name('comments.show');
Route::delete('comment/{comment}','CommentController@destroy')->name('comments.destroy');
Route::post('/comments/{id}','CommentController@update')->name('comments.update');

//Replies
Route::post('/books/{book}/comments/{comment}/reply','ReplyController@store')->name('reply.store');
Route::get('/books/{book}/comments/{comment}/reply','ReplyController@show')->name('replies.show');