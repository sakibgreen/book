@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!--Book show-->
			<div class="card text-center">
				<div class="card-header">
                    <h4><b> {{$books->title}} </b></h4>
                </div>
				<div class="card-block">
					<img src="{{ asset("storage/upload/" .$books->image_name)}}" width="400" height="400">
					<h5><b>Post By:</b> {{ App\Book::user_name($books->user_id) }} <h6>{{$books->updated_at->diffForHumans()}}</h6></h5>
					<p class="card-text">
						{{ $books->discription }}
					</p>
				</div>
			</div>
			<hr>
			<!--Comment Show-->
			<h3><b>User Recomment( {{$books->comments->count()}} )</b></h3>
			@foreach($comments as $comment)
				<h4>{{App\Book::user_name($comment->user_id)}} <h6>{{$comment->updated_at->diffForHumans()}}</h6> <h5>{{$comment->comments}}</h5></h4>
				<!--Comment Edit-->
				@if ($comment->user_id == auth()->user()->id)
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#{{$comment->id}}">
  						Edit
					</button>

					<!-- Modal -->
					<div class="modal fade" id="{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  						<div class="modal-dialog" role="document">
    						<div class="modal-content">
      							<div class="modal-header">
        							<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        							</button>
      							</div>
      							<div class="modal-body">
    					    		<form action="{{route('comments.update',$comment->id)}}" method="post">
    					    			{{csrf_field()}}
 		   					    		<textarea name="comment" id="{{$comment->id}}" class="form-control">{{$comment->comments}}</textarea>
    					    			<div class="modal-footer">
        									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        									<button type="submit" class="btn btn-primary">Save changes</button>
      									</div>
    					    		</form>
      							</div>
    						</div>
					  	</div>
					</div>
					<!--Comment Delete-->
					<form action="{{route('comments.destroy',$comment->id)}}"
						method="post">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button class="btn btn-danger btn-sm">Delete</button>
					</form>
				@elseif ($books->user_id == auth()->user()->id)
					<form action="{{route('comments.destroy',$comment->id)}}"
						method="post">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button class="btn btn-danger btn-sm">Delete</button>
					</form>
				@endif
				<!--Reply-->
				<!--Reply Show-->
				@foreach($replies as $reply)
				<div class="col-md-12"> 
					<div class="col-md-10">
						@if($reply->comment_id == $comment->id)
							<h4>{{App\Book::user_name($reply->user_id)}} <h6>{{$reply->updated_at->diffForHumans()}}</h6> <h5>{{$reply->replies}}</h5></h4>
						@endif
					</div>
				</div>
				@endforeach
				<!--Reply input-->
				<div class="col-md-12">
					<div class="col-md-8">
				<form class="form-horizontal mt5" action="/books/{{$books->id}}/comments/{{$comment->id}}/reply" method="post">
					{{csrf_field()}}
					<div class="form-group">
    					<label for="reply">Reply</label>
    					<input type="text" name="replies" class="form-control" id="reply" placeholder="Reply this comment">
						<button type="submit" class="btn btn-primary btn-sm">Submit</button><br>
  					</div>
  				</form>
  			</div>
  			</div>
			@endforeach
			<!--Comment Input field-->
			<h3>Give Up Comment</h3>
			<form class="form-horizontal mt5" action="/books/{{$books->id}}/comment" method="post">
				{{csrf_field()}}
				<div class="form-group required">
					<label for="comment">Comment</label>
					<textarea name="comment" id="comment" rows="3" class="form-control" type="text"></textarea>
				</div>
				<div class="clearfix">
					<button type="submit" class="btn btn-primary btn-sm btn-block">Submit</button><br>
				</div>
			</form>
		</div>
	</div>
	<a href="{{ url()->previous() }}" class="btn btn-outline-primary"> << Back</a>
</div>

@endsection