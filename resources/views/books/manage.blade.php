@extends('layouts.app')

@section('content')
<h1><center>All Books </center></h1>
<table class="table table-dark">
	<thead>
		<tr>
			<th scope="col">Number</th>
			<th scope="col">Title</th>
			<th scope="col">Image</th>
			<th scope="col">Discription</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($books as $book)
			@if ($book->user_id == auth()->user()->id)
			<tr>
				<td>{{ $loop->index+1 }}</td>
				<td>{{$book->title}}</td>
				<td><img src="{{ asset("storage/upload/".$book->image_name)}}" width="200" height="100"></td>
				<td>{{$book->discription}}</td>
				<td>
					<a href="{{route('books.edit',$book->id)}}" class="btn btn-warning" style="display: inline;">Edit</a>
					<form action="{{route('books.destroy',$book->id)}}" method="post">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button class="btn btn-denger btn-sm">Delete</button>
					</form>
				</td>
			</tr>
			@endif
		@endforeach
	</tbody>
</table>
@endsection