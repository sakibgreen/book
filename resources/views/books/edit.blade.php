@extends('layouts.app')

@section('content')

<div class="container"> 

	<form class="form-horizontal mt-5" action="{{route('books.update',['id'=>$books->id])}}" 
		method="post" enctype="multipart/form-data">
		{{method_field('PUT')}}
		{{csrf_field()}}
		<fieldset>
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-12 control-label" for="title">Title</label>  
			  <div class="col-md-12">
			  <input id="title" name="title" type="text" placeholder="Write your title" class="form-control input-md" required="" value="{{$books->title}}">
			    
			  </div>
			</div>

			<!--Image-->
			<input type="file" name="image_name">
		  	<img class="card-img-top" src="{{ asset("storage/upload/".$books->image_name)}}" width="200" height="200" alt="Card image cap">

			<!-- Textarea -->
			<div class="form-group">
			  <label class="col-md-12 control-label" for="discription">Discription</label>
			  <div class="col-md-12">                     
			    <textarea class="form-control" id="discription" name="discription">{{$books->discription}}</textarea>
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-12 control-label"></label>
			  <div class="col-md-12">
			    <button class="btn btn-outline-warning">Save</button>
			  </div>
			</div>
		</fieldset>
	</form>
</div>


@endsection