@extends('layouts.app')

@section('content')

<div class="container">
		<div class="row marketing">
			@foreach($all_posts as $book)
				<div class="col-lg-6">
					<h5 class="card-post-id"></h5>
		  			<img class="card-img-top" src="{{ asset("storage/upload/".$book->image_name)}}" width="100%" height="350" alt="Card image cap">
			  		<div class="card-body">
			    		<h4 class="card-title"><b>{{$book->title}}</b><h6><b>Post By:</b>{{App\Book::user_name($book->user_id)}}</h6></h4>
			    		<p class="card-text">{{ str_limit($book->discription, 300) }} <a href="{{route('books.show',$book->id)}}" > Read More.....</a></p>
				  	</div>
				</div>  	 		
			@endforeach
		</div>
		<!--paginate-->
        <div class="text-center">
            {!! $all_posts->links() !!}
        </div>
</div>

@endsection

