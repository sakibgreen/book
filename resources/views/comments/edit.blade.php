@extends('layouts.app')
@section('content')
<div class="container">
	<form class="form-horizontal mt5" action="{{route('comments.update',['id'=>$comments->id])}}" method="post">
		{{csrf_field()}}
		<div class="form-group required">
			<label for="comment">Comment</label>
			<textarea name="comment" id="comment" rows="3" class="form-control" type="text">{{$comments->comments}}</textarea>
		</div>
		<div class="clearfix">
			<button type="submit" class="btn btn-primary btn-sm btn-block">Submit</button><br>
		</div>
	</form>
</div>
@endsection