<?php

namespace App;

use App\User;
use App\Comment;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded=[];

    public function users()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public static function user_name($id)
    {
    	$name = User::find($id);
    	if (@$name) {
    		return $name->name;
    	}
    	else
    		return "";
    }
}
