<?php

namespace App;

use App\User;
use App\Book;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
	protected $guarder = [];

	public function users()
	{
    	return $this->belongsTo(User::class);
	}

	public function books()
	{
		return $this->belongsTo(Book::class);
	}

	public function comments()
	{
		return $this->belongsTo(Comment::class);
	}
}
