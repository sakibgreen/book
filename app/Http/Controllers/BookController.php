<?php

namespace App\Http\Controllers;

use App\Book;
use App\User;
use App\Comment;
use App\Reply;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => 'index']);
    }
    
    public function index()
    {
        $all_posts = Book::orderby('id', 'desc')->paginate(2);
        return view('books.index',compact('all_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image_name')){
            $image_name = $request->image_name->getClientOriginalName();
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);

            $books = new Book;
            $books->user_id         = auth()->user()->id;
            $books->title           = $request->title;
            $books->image_name      = $image_name;
            $books->image_size      = $image_size;
            $books->discription     = $request->discription;
            $books->save();      
        }
        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $books    = Book::find($id);
        $books_id = $books->id;
        $comments = Comment::where('book_id',$books_id)->get();
        // return $comments;
        $replies  = Reply::where('book_id',$books_id)->get();

        return view('books.show',compact('books','comments','replies','books_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function manage()
    {
        $books  = Book::all();
        return view('books.manage',compact('books'));
    }
    public function edit($id)
    {
        $books = Book::find($id);
        return view('books.edit',compact('books'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('image_name')){
            $image_name = $request->image_name->getClientOriginalName();
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);

            $books = Book::find($id);
            $books->update([
                'title'         => request('title'),
                'image_name'    => $image_name,
                'image_size'    => $image_size,
                'discription'   => request('discription')
            ]);
        }
        return redirect()->route('books.manage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $books = Book::find($id);
        $books->delete();
        return redirect()->route('books.manage');
    }
}
