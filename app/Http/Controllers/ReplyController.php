<?php

namespace App\Http\Controllers;
use App\Reply;
use App\Comment;
use App\Book;
use App\User;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function store(Request $request,Book $book ,Comment $comment)
    {
    	$replies = new Reply;
    	$replies->replies   	= $request->replies;
    	$replies->user_id		= auth()->user()->id;
    	$replies->comment_id	= $comment->id;
    	$replies->book_id   	= $book->id;
    	// dd($replies);
    	$replies->save();

    	return back();
    }

}
