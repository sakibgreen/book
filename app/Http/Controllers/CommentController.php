<?php

namespace App\Http\Controllers;

use App\User;
use App\Book;
use App\Comment;
use App\Reply;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request,Book $book)
    {
        $comments           = new Comment;
        $comments->comments = $request->comment;
        $comments->user_id  = auth()->user()->id;
        $comments->book_id  = $book->id;
        $comments->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment            = Comment::find($id);
        $book_id            = $comment->book_id;
        $comment->comments  = $request->comment;
        $comment->book_id   = $book_id;
        $comment->save();

        return redirect()->route('books.show',$book_id); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comment::find($id);
        $comments->delete();

        return back();
    }
}
