<?php

namespace App;

use App\User;
use App\Book;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    public function books()
    {
    	return $this->belongsTo(Book::class);
    }

    public function users()
    {
    	return $this->belongsTo(User::class);
    }

    public function replies()
    {
    	return $this->hasMany(Reply::class);
    }
}
